/* eslint-disable jsx-a11y/img-redundant-alt */
import React from 'react';
import profile from "./profile.jpg";

const Card = () => {
  return (
    <div
      style={{
        maxWidth: '100%', // Ensure the card doesn't exceed the container size
        boxSizing: 'border-box',
        padding: '16px',
        border: '1px solid #ccc',
        borderRadius: '8px',
        margin: '16px',
        textAlign: 'left',
        boxShadow: '0 4px 8px rgba(0, 0, 0, 0.1)',
        display: 'flex',
        justifyContent: 'center',
      }}
    >
      <img
        src={profile} // Replace with the URL of your image
        alt="Card Image"
        style={{
          maxWidth: '100%', // Ensure the image doesn't exceed the card width
          height: '518px', // Maintain the aspect ratio of the image
          borderRadius: '8px', // Match the card's border radius
          marginBottom: '16px', // Add some space between the image and text
        }}
      />
      
    </div>
  );
};

export default Card;
