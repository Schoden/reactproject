import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';

// import profile from "./myDoct.jpg";
// import HeartButton from'./likebutton';
// import cross from "./cross.svg"
import exclamation from "./exc.svg"
import image1 from "../Landing/image1.png";
import image2 from "../Landing/image2.png";
// import profile from "./profile.jpg";
import Example from './DeleteModal';
import Card from './card'
import './style.css';

function ReportedProfile(){
    return(
        <>
        <div className="container" style={{ height: 'fit-content' }}>
        <div className="row align-items-start">
          <div className="col" style={{display: 'flex', flexDirection: 'column', height: '100%'}}>
            <div className="container-fluid" style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' , minHeight: '100%'}}>
            <ul className="list-unstyled" style={{ display: 'flex', flexDirection: 'column', textAlign:'left !important' }}>
                <li style={{textAlign:'left !important'}}><p className="h1">David Beckam</p></li>
                <li style={{textAlign:'left !important'}}><p class="lead" style={{color: 'black', textAlign:'left !important'}}>(A short message from your side) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p></li>
                <li style={{textAlign:'left !important'}}><p className="h3">Biography</p></li>
                <li style={{textAlign:'left !important'}}><p className="lead" style={{color: 'black'}}>(About yourself) Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p></li>
              </ul>
            </div>
             
              <div className="container-fluid" style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
              <Example></Example>
              <div className="text-danger">
                <img src={exclamation} alt="..."/> 2 times
                </div>
              </div>
          </div>
          <div className="col">
            {/* <img src={profile} className="img-fluid profileIMG" alt="profile" style={{ height: '10%', width: '80%' }} /> */}
            <Card></Card>
          </div>
          <hr></hr>
        </div>

        {/* <div className="container" style={{height: '100vh !important'}}>
          <div className="container text-center">
            <div className="row align-items-start">
              <div className="col-8">
                <div className="container text-center">
                  <div className="row align-items-start">
                    <div className="col">
                      <div className="card text-bg-dark">
                        <img src={img2} className="card-img" alt="..."/>
                        <div className="card-img-overlay">
                          <h5 className="card-title">Card title</h5>
                          <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                          <p className="card-text"><small>Last updated 3 mins ago</small></p>
                        </div>
                      </div>                  </div>
                    <div className="container text-center mt-3">
                      <div className="row align-items-start">
                        <div className="col" >
                          <div className="card text-bg-dark">
                            <img src={img2} className="card-img" alt="..."/>
                            <div className="card-img-overlay">
                              <h5 className="card-title">Card title</h5>
                              <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                              <p className="card-text"><small>Last updated 3 mins ago</small></p>
                            </div>
                          </div>                      </div>
                        <div className="col">
                          <div className="card text-bg-dark">
                            <img src={img1} className="card-img" alt="..." style={{height: '40% !important', width: '100% !important'}}/>
                            <div className="card-img-overlay">
                              <h5 className="card-title">Card title</h5>
                              <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                              <p className="card-text"><small>Last updated 3 mins ago</small></p>
                            </div>
                          </div>                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
  
  
              <div className="col-4">
                <div className="row">
                  <div className="col">
                    <div className="card text-bg-dark">
                      <img src={img1} className="card-img" alt="..."/>
                      <div className="card-img-overlay">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p className="card-text"><small>Last updated 3 mins ago</small></p>
                      </div>
                    </div>                </div>
                  <div className="w-100"></div>
                  <div className="col mt-3">
                    <div className="card text-bg-dark">
                      <img src={img1} className="card-img" alt="..."/>
                      <div className="card-img-overlay">
                        <h5 className="card-title">Card title</h5>
                        <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                        <p className="card-text"><small>Last updated 3 mins ago</small></p>
                      </div>
                    </div>                </div>
                </div>
              </div>
              
            </div>
          </div>
        </div> */}
        <div className="image-grid" style={{justifyContent: 'center'}}>
 
 <div className="shorter-square-image">
   {/* Add your shorter square-shaped image here */}
   <Link to={'/viewPost'}>
   <img src={image2} alt="1" />
   </Link>
 </div>
 <div className="taller-image">
   {/* Add your taller image here */}
   <Link to={'/viewPost'}>
   <img src={image1} alt="2" />
   </Link>
 </div>
 <div className="shorter-square-image">
   {/* Add another shorter square-shaped image here */}
   <Link to={'/viewPost'}>
   <img src={image2} alt="5" />
   </Link>
 </div>
 <div className="taller-image2">
   {/* Add your taller image here */}
   <Link to={'/viewPost'}>
   <img src={image1} alt="2" />
   </Link>
 </div>
 <div className="shorter-square-image">
   {/* Add another shorter square-shaped image here */}
   <Link to={'/viewPost'}>
   <img src={image2} alt="5" />
   </Link>
 </div>
 <div className="taller-image2">
   <Link to={'/viewPost'}>
   <img src={image1} alt="2" />
   </Link>
 </div>
 {/* Add more grid items as needed */}
</div>
      </div>
      </>
    )
}


export default ReportedProfile;