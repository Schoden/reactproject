import { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import Cross from './cross.svg'

function Example() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="danger" onClick={handleShow}>
        Delete User
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body style={{textAlign: 'center', fontSize: '35px'}}>Are you sure<br></br>You want to delete the user?</Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={handleClose}>
            No
          </Button>
            <Example2 onClose={handleClose} />
        </Modal.Footer>
      </Modal>
    </>
  );
}

function Example2({ onClose }) {
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    onClose(); // Call the onClose prop to close the first modal
  };

  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant="danger" onClick={handleShow}>
        Yes
      </Button>

      <Modal show={show} onHide={handleClose} animation={true}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body style={{textAlign: 'center', fontSize: '35px'}}>User Deleted<br></br><img src={Cross} alt='001' /></Modal.Body>
        <Modal.Footer>
          <Button variant="success" onClick={handleClose}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
}

export default Example;
