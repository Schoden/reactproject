import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import profile from "./myDoct.jpg";
import HeartButton from'./likebutton';
import { Link } from 'react-router-dom';
import './report'
import './style.css';


function Admin() {
  return (
    <div className="container-fluid mt-5">
<div class="container-fluid col-md-11 card mb-3">
        <div class="card-header">STATISTICS</div>
        <div class="card-body">
          <div class="row">
            <div class="col">
              <div class="card-stat Cbg1">
                <h5 class="card-title text-uppercase mb-0">Patients</h5>
                <span class="h2 font-weight-bold mb-0">267</span>
              </div>
            </div>
            <div class="col">
              <div class="card-stat Cbg2">
                <h5 class="card-title text-uppercase mb-0">Doctors</h5>
                <span class="h2 font-weight-bold mb-0">67</span>
              </div>
            </div>
            <div class="col">
              <div class="card-stat Cbg3">
                <h5 class="card-title text-uppercase mb-0">Reports</h5>
                <span class="h2 font-weight-bold mb-0">26</span>
              </div>
            </div>
            <div class="col">
              <div class="card-stat Cbg3">
                <h5 class="card-title text-uppercase mb-0">Reports</h5>
                <span class="h2 font-weight-bold mb-0">26</span>
              </div>
            </div>
          </div>
        </div>
      </div>

      <main class="row">
        <section class="container-fluid col-md-11">
          <div class="card mb-3">
            <div class="card-header d-flex flex-row justify-content-between align-items-center">
              <h5 class="card-title text-uppercase mb-0">REPORTS</h5>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                        <td><img src={profile} class="img-fluid" alt='...'/> Commentator</td>
                      <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                      <td class="text-end">
                      <Link to="/report">
                          <div class="btn btn-sm btn-primary">View Profile</div>
                          </Link></td>
                    </tr>
                    <tr>
                        <td><img src={profile} class="img-fluid" alt='...'/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                        <Link to="/report">
                          <div class="btn btn-sm btn-primary">View Profile</div>
                          </Link></td>
                    </tr>
                    <tr>
                        <td><img src={profile} class="img-fluid" alt='...'/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                        <Link to="/report">
                          <div class="btn btn-sm btn-primary">View Profile</div>
                          </Link></td>
                    </tr>
                    <tr>
                        <td><img src={profile} class="img-fluid" alt='...'/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                        <Link to="/report">
                          <div class="btn btn-sm btn-primary">View Profile</div>
                          </Link></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
      </main>
      <main class="row">
        <section class="container-fluid col-md-11">
          <div class="card mb-3">
            <div class="card-header d-flex flex-row justify-content-between align-items-center">
              <h5 class="card-title text-uppercase mb-0">REPORTS</h5>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-hover">
                  <tbody>
                    <tr>
                        <td><img src={profile} alt='img' class="img-fluid"/> Commentator</td>
                      <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                      <td class="text-end">
                      <div class="btn btn-sm btn-primary">View Profile</div></td>
                    </tr>
                    <tr>
                        <td><img src={profile} alt='img' class="img-fluid"/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                        <div class="btn btn-sm btn-primary">View Profile</div></td>
                    </tr>
                    <tr>
                        <td><img src={profile} alt='img' class="img-fluid"/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                        <div class="btn btn-sm btn-primary">View Profile</div></td>
                    </tr>
                    <tr>
                        <td><img src={profile} alt='img' class="img-fluid"/> Commentator</td>
                        <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem</td>
                        <td class="text-end">
                          <div class="btn btn-sm btn-primary">View Profile</div></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </section>
        <div>
          <HeartButton></HeartButton>
        </div>
      </main>



    </div>    
  );
}

export default Admin;





