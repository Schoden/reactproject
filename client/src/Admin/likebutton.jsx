import React, { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHeart } from "@fortawesome/free-solid-svg-icons";
import './style.css'; // Create a CSS file for styling

function HeartButton() {
  const [liked, setLiked] = useState(false);
  const [likeCount, setLikeCount] = useState(0);

  const handleLikeClick = () => {
    if (liked) {
      setLikeCount(likeCount - 1);
    } else {
      setLikeCount(likeCount + 1);
    }
    setLiked(!liked);
  };

  return (
    <div>
      <button
        className={`heart-button ${liked ? "liked" : ""}`}
        onClick={handleLikeClick}
      >
        <FontAwesomeIcon icon={faHeart} className="heart-icon" />
      </button>
      <span className="like-count">{likeCount} Likes</span>
    </div>
  );
}

export default HeartButton;
